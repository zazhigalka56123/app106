package com.sports.sob.newsmatch.centr.ssn.news

import android.content.Context
import android.webkit.WebSettings
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiNewsBuilder {
    fun basrwerafsf(context: Context, url: String): ApiNewsService {
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest: Request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("X-ACCESS-KEY", "pub_370072918be0c76f9bbe274dd142a188bcd08")
                .addHeader("User-Agent", WebSettings.getDefaultUserAgent(context).toString())
                .build()
            chain.proceed(newRequest)
        }.build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create()
    }

}