package com.sports.sob.newsmatch.centr.ssn.api

import android.content.Context
import com.sports.sob.newsmatch.centr.ssn.Match

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class ApiMatchRepositoryImpl(context: Context, url: String) : ApiMatchRepository {


    private val service = ApiMatchBuilder().asdsadsadasoijqwqeqw81231232141(context, url)


    override suspend fun GETT1(date: String, league: Int, season: String, timezone: String): Flow<List<Match>> = flow {
        with(service.asdasdsadasdjsfbasbfkasbfjabsfjbasjlfbjasbfjbafjl(date, league, season, timezone)) {
            val fdsfds = mutableListOf<Match>()
            if (isSuccessful){
                body()?.response?.forEach { response ->
                    fdsfds.add(Mapper().map1(response))
                }
            }
            emit(fdsfds.toList())
        }
    }.catch {
        emit(emptyList())
    }
        .flowOn(Dispatchers.IO)



    override suspend fun GETT2(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<Match>> = flow {
        with(service.fsafasfasfas(date,
            season, timezone)) {
            val list = mutableListOf<Match>()
            if (isSuccessful){
                body()?.response?.forEach { response ->
                    list.add(Mapper().map3(response))
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)


    override suspend fun GETT3(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<Match>> = flow {
        with(service.fsabjfbasjkfbkasbfkabkqweqweqweqweqwe3(date,
            timezone)) {
            val list = mutableListOf<Match>()
            if (isSuccessful){
                body()?.response?.forEach { response ->
                    list.add(Mapper().map2(response))
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }.flowOn(Dispatchers.IO)


}