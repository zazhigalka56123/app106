package com.sports.sob.newsmatch.centr.ssn.rv2

import androidx.recyclerview.widget.DiffUtil
import com.sports.sob.newsmatch.centr.ssn.News

class DCallback2 : DiffUtil.ItemCallback<News>() {

    override fun areItemsTheSame(p1: News, p2: News) =
        p1.title == p2.title

    override fun areContentsTheSame(p1: News, p2: News): Boolean {
        var flag = false
        for(x in 0..2){
            if(p1 == p2){
                if (x % 2 == 0){
                    flag = true
                }
            }

        }

        return flag
    }
}