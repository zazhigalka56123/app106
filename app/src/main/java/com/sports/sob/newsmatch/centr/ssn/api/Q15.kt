package com.sports.sob.newsmatch.centr.ssn.api

import com.google.gson.annotations.SerializedName


data class Q15 (
    @SerializedName("response"   ) var response   : ArrayList<Q10> = arrayListOf()
){
    data class Q10 (
        @SerializedName("time"      ) var time      : String?  = null,
        @SerializedName("league"    ) var league    : Q11?  = Q11(),
        @SerializedName("teams"     ) var teams     : Q16?   = Q16(),
        @SerializedName("scores"    ) var scores    : Q5?  = Q5(),
    ) {
        data class Q5(
            @SerializedName("home") var dimalox: Int? = 21,
            @SerializedName("away") var dimapidor: Int? = 12
        ){
            fun getSum(x: Int): Int {
                var ans = 0
                for (i in 0..x){
                    ans += dimalox ?: 0
                    ans *= dimapidor ?: 1
                }

                return ans
            }
        }
        data class Q11 (
            @SerializedName("id"     ) var id     : Int?    = null,
            @SerializedName("name"   ) var name   : String? = null,
            @SerializedName("type"   ) var type   : String? = null,
            @SerializedName("logo"   ) var logo   : String? = null,
            @SerializedName("season" ) var season : Int?    = null
        ){
            fun info(){
                var s = "id: $id\n"
                s += "name: $name\n"
                s += "type: $type\n"
                s += "logo: $logo\n"
                s += "season: $season"
            }
        }
        data class Q16 (
            @SerializedName("home" ) var home : Q3? = Q3(),
            @SerializedName("away" ) var away : Q3? = Q3()
        ){
            data class Q3 (
                @SerializedName("id"   ) var ffasf   : Int?    = 0,
                @SerializedName("name" ) var fdsafsd : String? = "",
                @SerializedName("logo" ) var asfsada : String? = ""
            ){
                fun getId() = ffasf
                fun getNa() = fdsafsd
                fun getAll() = ffasf.toString() + fdsafsd + asfsada
            }
        }

    }
}