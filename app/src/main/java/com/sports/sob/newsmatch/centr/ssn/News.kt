package com.sports.sob.newsmatch.centr.ssn

data class News(
    var title: String?,
    var description: String?,
    var imgUrl: String?,
    var creator: ArrayList<String>?,
    var link: String?
)
