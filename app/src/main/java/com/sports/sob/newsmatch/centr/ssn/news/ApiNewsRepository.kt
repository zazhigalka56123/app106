package com.sports.sob.newsmatch.centr.ssn.news

import com.sports.sob.newsmatch.centr.ssn.News


import kotlinx.coroutines.flow.Flow

interface ApiNewsRepository {

    suspend fun getgewtfasfsagadgNsfasfsafasews(
        count: Int,
        language: String,
        apikey: String,
    ): Flow<List<News>>
}