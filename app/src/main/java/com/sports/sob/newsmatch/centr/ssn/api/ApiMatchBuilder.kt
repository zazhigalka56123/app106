package com.sports.sob.newsmatch.centr.ssn.api

import android.content.Context
import android.webkit.WebSettings
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class ApiMatchBuilder {
    fun asdsadsadasoijqwqeqw81231232141(context: Context, url: String): ApiMatchService {
        return Retrofit.Builder()
            .client(OkHttpClient.Builder().addInterceptor { chain ->
                val newRequest: Request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .addHeader("x-rapidapi-key", "edde29dee5aac69bd806d2c8edec2a0d")
                    .addHeader("User-Agent", WebSettings.getDefaultUserAgent(context).toString())
                    .build()
                chain.proceed(newRequest)
            }.build())
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

}