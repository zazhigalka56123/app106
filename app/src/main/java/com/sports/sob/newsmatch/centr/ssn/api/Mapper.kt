package com.sports.sob.newsmatch.centr.ssn.api

import com.sports.sob.newsmatch.centr.ssn.Match
import com.sports.sob.newsmatch.centr.ssn.api.A4.A10
import java.text.SimpleDateFormat
import java.util.Calendar

class Mapper {

    fun map1(f6: F7.F6): Match {
        return  Match(
            t1 = f6.fdsfdsfsd?.dssfdfsf?.fsfsa.toString(),
            t2 = f6.fdsfdsfsd?.dasdas?.fsfsa.toString(),
            time = f6.time.toString(),
            logo1 = f6.fdsfdsfsd?.dssfdfsf?.fadsf.toString(),
            logo2 = f6.fdsfdsfsd?.dasdas?.fadsf.toString(),
            asfasfasf = f6.scores?.home?.safasf?.toString()?.toInt(),
            sdwqsdsa = f6.scores?.fasfa?.safasf?.toString()?.toInt(),
            league = f6.league?.asfsa.toString()
        )
    }


    fun map2(x: Q15.Q10): Match {
        with(x) {
            return Match(
                t1 = teams?.home?.fdsafsd.toString(),
                t2 = teams?.away?.fdsafsd.toString(),
                time  =  time.toString(),
                logo1 = teams?.home?.asfsada.toString(),
                logo2 = teams?.away?.asfsada.toString(),
                asfasfasf = scores?.dimalox,
                sdwqsdsa = scores?.dimapidor,
                league = this.league?.name.toString()

            )
        }
    }

    fun map3(x: A10): Match {
        with(x) {
            val str = x.fixture?.date.toString()
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val d = format.parse(str.substring(0, 18))
            val c = Calendar.getInstance()
            c.time = d
            var h = c.get(Calendar.HOUR_OF_DAY).toString()
            if (h.length == 1){
                h = "0$h"
            }
            var t = c.get(Calendar.MINUTE).toString()
            if (t.length == 1){
                t = "0$t"
            }
            val s =  "$h:$t"

            return Match(
                t1 = teams?.home?.dasdasda.toString(),
                t2 = teams?.away?.dasdasda.toString(),
                time = s,
                logo1 = teams?.home?.adssa.toString(),
                logo2 = teams?.away?.adssa.toString(),
                asfasfasf = goals?.fsfsdsf?.toInt(),
                sdwqsdsa = goals?.fasdas?.toInt(),
                league = this.league?.ogogogo.toString()

            )
        }
    }

}