package com.sports.sob.newsmatch.centr.ssn.news

import android.content.Context
import com.sports.sob.newsmatch.centr.ssn.News
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class ApiNewsRepositoryImpl(context: Context) : ApiNewsRepository {
    private val service = ApiNewsBuilder().basrwerafsf(context, "https://newsdata.io/")

    override suspend fun getgewtfasfsagadgNsfasfsafasews(count: Int, language: String, apikey: String): Flow<List<News>> = flow {
        with(service.afafsaffaafafafa123ddafssa(count, language, apikey)) {
            val list = mutableListOf<News>()
            if (isSuccessful){
                if ((body()?.totalResults ?: 0) > 0) {
                    body()?.results?.forEach { response ->
                        with(response) {
                            val el = News(
                                title = this.title,
                                description = this.description,
                                imgUrl = this.imageUrl,
                                creator = this.creator,
                                link = this.link

                            )
                            list.add(el)
                        }
                    }
                }
            }
            emit(list.toList())
        }
    }.catch {
        emit(emptyList())
    }
    .flowOn(Dispatchers.IO)
}