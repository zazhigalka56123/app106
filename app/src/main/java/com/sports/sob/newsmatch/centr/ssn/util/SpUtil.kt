package com.sports.sob.newsmatch.centr.ssn.util

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class SpUtil(activity: Activity) {

    private val sp: SharedPreferences = activity.getSharedPreferences(Data.SP_KEY, Context.MODE_PRIVATE)

    fun getLink() = sp.getString(Data.MAIN_KEY, "").toString()
    fun getLinkIsSaved() = sp.getBoolean(Data.IS_GET_KEY, false)

    fun setLink(link: String) {
        sp.edit {
            putString(Data.MAIN_KEY, link)
            commit()
        }
    }
    fun setLinkSaved(flag: Boolean) {
        sp.edit {
            putBoolean(Data.IS_GET_KEY, flag)
            commit()
        }
    }
}