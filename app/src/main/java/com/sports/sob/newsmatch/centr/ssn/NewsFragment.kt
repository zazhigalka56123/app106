package com.sports.sob.newsmatch.centr.ssn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.sports.sob.newsmatch.centr.ssn.databinding.FragmentNewsBinding
import com.sports.sob.newsmatch.centr.ssn.news.ApiNewsRepositoryImpl
import com.sports.sob.newsmatch.centr.ssn.rv2.MAdapter2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsFragment: Fragment() {

    private var day = 0
    private lateinit var view: FragmentNewsBinding
    private val list: MutableLiveData<List<News>> = MutableLiveData(listOf())
    private val a = MAdapter2()

    private val rep by lazy {
        ApiNewsRepositoryImpl(requireContext())
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        view = FragmentNewsBinding.inflate(inflater, container, false)

        view.list.adapter = a
        view.list.layoutManager = LinearLayoutManager(requireContext())

        list.observeForever {
            a.submitList(it)
        }

        gsgsgsngknssgkdngsl()


        return view.root
    }


    private fun gsgsgsngknssgkdngsl() {
        view.list.visibility = View.INVISIBLE
        lifecycleScope.launch(Dispatchers.IO) {
            requireActivity().runOnUiThread {
                view.oiudfsdf.visibility = View.VISIBLE
            }
            val l = mutableListOf<News>()
            rep.getgewtfasfsagadgNsfasfsafasews(10, "ru", "pub_370072918be0c76f9bbe274dd142a188bcd08").collect { response ->
                l.addAll(response)
                list.postValue(l)
            }

            requireActivity().runOnUiThread {
                view.oiudfsdf.visibility = View.GONE
                view.list.visibility = View.VISIBLE

            }
        }
    }
}