package com.sports.sob.newsmatch.centr.ssn.api

import com.google.gson.annotations.SerializedName


data class F7 (
    @SerializedName("results"    ) var results    : Int?                = null,
    @SerializedName("response"   ) var response   : ArrayList<F6> = arrayListOf()
){
    data class F6 (
        @SerializedName("id"        ) var id        : Int?     = 0,
        @SerializedName("time"      ) var time      : String?  = "",
        @SerializedName("teams"     ) var fdsfdsfsd : F3?   = null,
        @SerializedName("scores"    ) var scores    : F4?  = F4(),
        @SerializedName("league"    ) var league    : F1?  = F1()
    )

    data class F1 (
        @SerializedName("name") var asfsa: String? = null
    ){
        fun str(): String {
            return asfsa ?: ""
        }
    }
    data class F3 (
        @SerializedName("home" ) var dssfdfsf : F8? = F8(),
        @SerializedName("away" ) var dasdas : F8? = F8()
    ){
        data class F8 (
            @SerializedName("id"   ) var fsdfsd   : Int?    = null,
            @SerializedName("name" ) var fsfsa : String? = null,
            @SerializedName("logo" ) var fadsf : String? = null
        ){
            fun toStr(): String {
                return "id $fsdfsd name $fsfsa logo $fadsf"
            }

        }

        fun makeNull(){
            dssfdfsf = null
            dasdas = null
        }
        fun score(): String {
            return dssfdfsf?.fsfsa + " " + (dasdas?.fsfsa ?: "")
        }
    }
    data class F4 (
        @SerializedName("home" ) var home : F5? = F5(),
        @SerializedName("away" ) var fasfa : F5? = F5()

    ){
        fun makeNull(){
            home = null
            fasfa = null
        }
        fun sfszf(): String {
            return "${home?.safasf.toString()} : ${home?.safasf.toString() ?: "-"}"
        }

        data class F5 (
            @SerializedName("total") var safasf: Int? = null
        ){
            fun gete(): Boolean{
                return (safasf ?: 0) > 0
            }
        }

    }

}