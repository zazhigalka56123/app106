package com.sports.sob.newsmatch.centr.ssn

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sports.sob.newsmatch.centr.ssn.databinding.ActivityMainBinding

class  ActivityForProductivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val x = ActivityMainBinding.inflate(layoutInflater)
        setContentView(x.root)
    }
}