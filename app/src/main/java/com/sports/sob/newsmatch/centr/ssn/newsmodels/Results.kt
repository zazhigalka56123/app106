package com.sports.sob.newsmatch.centr.ssn.newsmodels

import com.google.gson.annotations.SerializedName


data class Results (

  @SerializedName("article_id"      ) var articleId      : String?           = null,
  @SerializedName("title"           ) var title          : String?           = null,
  @SerializedName("link"            ) var link           : String?           = null,
  @SerializedName("keywords"        ) var keywords       : ArrayList<String> = arrayListOf(),
  @SerializedName("creator"         ) var creator        : ArrayList<String> = arrayListOf(),
  @SerializedName("video_url"       ) var videoUrl       : String?           = null,
  @SerializedName("description"     ) var description    : String?           = null,
  @SerializedName("content"         ) var content        : String?           = null,
  @SerializedName("pubDate"         ) var pubDate        : String?           = null,
  @SerializedName("image_url"       ) var imageUrl       : String?           = null,
  @SerializedName("source_id"       ) var sourceId       : String?           = null,
  @SerializedName("source_url"      ) var sourceUrl      : String?           = null,
  @SerializedName("source_priority" ) var sourcePriority : Int?              = null,
  @SerializedName("country"         ) var country        : ArrayList<String> = arrayListOf(),
  @SerializedName("category"        ) var category       : ArrayList<String> = arrayListOf(),
  @SerializedName("language"        ) var language       : String?           = null,
  @SerializedName("ai_tag"          ) var aiTag          : String?           = null,
  @SerializedName("sentiment"       ) var sentiment      : String?           = null,
  @SerializedName("sentiment_stats" ) var sentimentStats : String?           = null,
  @SerializedName("ai_region"       ) var aiRegion       : String?           = null

)