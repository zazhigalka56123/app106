package com.sports.sob.newsmatch.centr.ssn

import android.app.Application
import com.backendless.Backendless
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig

class App106: Application() {

    override fun onCreate() {
        super.onCreate()
        val config = YandexMetricaConfig.newConfigBuilder("e1c28164-cb6f-4c3f-a5c2-ab3b5175f602").build()
        YandexMetrica.activate(applicationContext, config)
        YandexMetrica.enableActivityAutoTracking(this)

        Backendless.setUrl("https://api.backendless.com")
        Backendless.initApp(applicationContext, "C8F88CFC-56FA-5265-FF5F-6540F5BD6300", "5E99CB40-9482-4DA7-8EBA-4605737AE769")
    }
}