package com.sports.sob.newsmatch.centr.ssn.api

import com.google.gson.annotations.SerializedName
import java.util.Random


data class A4 (
  @SerializedName("response"   ) var response   : ArrayList<A10> = arrayListOf()
){
  data class A10 (
    @SerializedName("fixture" ) var fixture : A1? = A1(),
    @SerializedName("league"  ) var league  : A5?  = A5(),
    @SerializedName("teams"   ) var teams   : A9?   = A9(),
    @SerializedName("goals"   ) var goals   : A6?   = A6(),
    ){
    data class A1 (
      @SerializedName("date"      ) var date      : String?  = null,
    )
    data class A5 (
      @SerializedName("id"      ) var fasasf      : Int?    = null,
      @SerializedName("name"    ) var ogogogo    : String? = null
    ){
      fun idToStr(): String {
        return fasasf.toString()
      }
      fun getNameWith(): String{
        return "$ogogogo $fasasf"
      }
    }
    data class A6 (
      @SerializedName("home" ) var fsfsdsf : String? = null,
      @SerializedName("away" ) var fasdas : String? = null
    ){

      fun formattedString(): String {
        return "$fsfsdsf : $fasdas"
      }

    }
    data class A9 (

      @SerializedName("home" ) var home : A7? = A7(),
      @SerializedName("away" ) var away : A7? = A7()
    ){
      data class A7 (
        @SerializedName("id"     ) var sdfdsds     : Int?    = null,
        @SerializedName("name"   ) var dasdasda   : String? = null,
        @SerializedName("logo"   ) var adssa   : String? = null,
        @SerializedName("winner" ) var adasda : String? = null
      ){
        fun reid(){
          sdfdsds = Random().nextInt(1000000)
        }
        fun getWinner(): String {
          return adasda?: "not found"
        }
      }
    }
  }
}