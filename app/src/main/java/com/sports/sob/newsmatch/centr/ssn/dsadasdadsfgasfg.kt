package com.sports.sob.newsmatch.centr.ssn

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.backendless.Backendless
import com.sports.sob.newsmatch.centr.ssn.databinding.FragmentStartBinding
import com.sports.sob.newsmatch.centr.ssn.util.Data.objectId
import com.sports.sob.newsmatch.centr.ssn.util.Data.privacyLink
import com.sports.sob.newsmatch.centr.ssn.util.Data.tableName
import com.sports.sob.newsmatch.centr.ssn.util.SpUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.HttpURLConnection
import java.net.URL

class dsadasdadsfgasfg: Fragment() {

    var flag = false
    private lateinit var b: FragmentStartBinding
    private val db by lazy {
        SpUtil(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = FragmentStartBinding.inflate(inflater, container, false)

        animate()
        get()
        return b.root
    }

    private fun animate(){
        b.adasdasdad.playAnimation()
        b.adasdasdad.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
            }

            override fun onAnimationEnd(animation: Animator) {
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
                if (flag) {
                    db.setLinkSaved(true)
                    findNavController().navigate(R.id.fsdmbfdnsbfds)
                }
            }
        })
    }

    private fun get(){
        if (!db.getLinkIsSaved()) {
            lifecycleScope.launch(Dispatchers.IO) {
                try {
                    val x = Backendless.Data.of(tableName).findById(objectId)[tableName]

                    if (x == null) {
                        db.setLink("null")
                        flag = true
                    } else {
                        if (gettt(x.toString())?.contains(privacyLink) == true) {
                            db.setLink("null")
                        } else {
                            val last = gettt(x.toString())
                            if (last != null) {
                                db.setLink(last.toString())
                            } else {
                                db.setLink("null")
                            }
                        }
                    }
                    flag = true
                } catch (e: Exception) {
                    db.setLink("null")
                    flag = true
                }
            }
        }else{
            findNavController().navigate(R.id.fsdmbfdnsbfds)
        }
    }

    private fun gettt(url: String?): String? {
        val fsfdsfdsfsd = (URL(url).openConnection() as HttpURLConnection).apply {
            instanceFollowRedirects = false
            connect()
        }
        if (fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
            val fdsfdsfds = fsfdsfdsfsd.getHeaderField("Location")
            return gettt(fdsfdsfds)
        }
        if(fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_TEMP){
            return gettt(fsfdsfdsfsd.getHeaderField("Location"))
        }
        return url
    }
}