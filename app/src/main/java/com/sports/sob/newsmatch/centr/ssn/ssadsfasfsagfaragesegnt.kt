package com.sports.sob.newsmatch.centr.ssn

import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.sports.sob.newsmatch.centr.ssn.api.ApiMatchRepositoryImpl
import com.sports.sob.newsmatch.centr.ssn.databinding.FragmentMatchesBinding
import com.sports.sob.newsmatch.centr.ssn.recyclerview.dasadadasdasad
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Date

class ssadsfasfsagfaragesegnt: Fragment() {

    private var day = 0
    private lateinit var view: FragmentMatchesBinding
    private val list: MutableLiveData<List<Match>> = MutableLiveData(listOf())
    private val a = dasadadasdasad()

    private val rep0 by lazy {
        ApiMatchRepositoryImpl(requireContext(), "https://v3.football.api-sports.io/")
    }
    private val rep1 by lazy {
        ApiMatchRepositoryImpl(requireContext(), "https://v1.basketball.api-sports.io/")
    }
    private val rep2 by lazy {
        ApiMatchRepositoryImpl(requireContext(), "https://v1.volleyball.api-sports.io")
    }

    private lateinit var rep: ApiMatchRepositoryImpl
    var gameId = 0
    private val timezone = Calendar.getInstance().timeZone.toZoneId().toString()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        view = FragmentMatchesBinding.inflate(inflater, container, false)

        rep = rep0
        view.list.adapter = a
        view.list.layoutManager = LinearLayoutManager(requireContext())

        list.observeForever {
            a.submitList(it)
        }

        val date = DateFormat.format("yyyy-MM-dd", Date()).toString()
        dassadsadkkhfhkdffdhdfjdggududdu(date)

        view.dsfsdfsd.setOnClickListener {
            if (day >= 0){
                day -= 1
                view.forward.backgroundTintList = (ContextCompat.getColorStateList(requireActivity(), R.color.wfjafanaksfsa))

                if (day == -1)
                    view.dsfsdfsd.backgroundTintList = (ContextCompat.getColorStateList(requireActivity(), R.color.notactive))

                setDates()
            }
        }

        view.forward.setOnClickListener {
            if (day <= 3){
                day += 1
                view.dsfsdfsd.backgroundTintList = (ContextCompat.getColorStateList(requireActivity(), R.color.wfjafanaksfsa))

                if (day == 4)
                    view.forward.backgroundTintList = (ContextCompat.getColorStateList(requireActivity(), R.color.notactive))

                setDates()
            }
        }

        val data = arrayOf("Футбол", "Баскетбол", "Волейбол")

        val adapter = ArrayAdapter(requireContext(), R.layout.spinner_item_selected, R.id.txt,  data)
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)

        val spinner = view.spinner
        spinner.adapter = adapter
        spinner.setSelection(0, false )
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                gameId = position
                rep = when (gameId){
                    0 -> rep0
                    1 -> rep1
                    else -> rep2
                }
                val c = Calendar.getInstance()
                c.add(Calendar.DAY_OF_MONTH, day)
                dassadsadkkhfhkdffdhdfjdggududdu(DateFormat.format("yyyy-MM-dd", c.time).toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }


        return view.root
    }

    private fun setDates(){
        val c = Calendar.getInstance()
        c.add(Calendar.DAY_OF_MONTH, day)

        view.date0.text = setTextDate(day - 1)
        view.date1.text = setTextDate(day)
        view.date2.text = setTextDate(day + 1)

        dassadsadkkhfhkdffdhdfjdggududdu(DateFormat.format("yyyy-MM-dd", c.time).toString())
    }
    private fun setTextDate(day: Int): String{
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, day)
        return when (day){
            -2 -> ""
            -1 -> "Вчера"
            0 -> "Сегодня"
            1 -> "Завтра"
            5 -> ""
            else -> DateFormat.format("dd.MM", calendar.time).toString()
        }

    }

    private fun dassadsadkkhfhkdffdhdfjdggududdu(date: String){
        view.forward.isClickable = false
        view.dsfsdfsd.isClickable = false
        view.list.visibility = View.INVISIBLE
        lifecycleScope.launch(Dispatchers.IO) {
            requireActivity().runOnUiThread {
                view.dasas.visibility = View.VISIBLE
            }
            val l = mutableListOf<Match>()
            when(gameId){
                0 -> {
                    rep.GETT2(date, 1, "2024", timezone).collect { response ->
                        l.addAll(response)
                        list.postValue(l)
                    }
                }
                1 -> {
                    rep.GETT1(date, 12, "2023-2024", timezone).collect { response ->
                        l.addAll(response)
                        list.postValue(l)
                    }
                    rep.GETT1( date, 217, "2023-2024", timezone).collect { response ->
                        l.addAll(response)
                        list.postValue(l)
                    }
                    rep.GETT1( date, 81, "2023-2024", timezone).collect { response ->
                        l.addAll(response)
                        list.postValue(l)
                    }
                }
                else -> {
                    rep.GETT3(date, 1, "2024", timezone).collect { response ->
                        l.addAll(response)
                        list.postValue(l)
                    }
                }
            }
            requireActivity().runOnUiThread {
                view.dasas.visibility = View.GONE
//                if (list.value?.isEmpty() == true)
//                    view.zero.visibility = View.VISIBLE
//                else
                    view.list.visibility = View.VISIBLE
                view.forward.isClickable = true
                view.dsfsdfsd.isClickable = true

            }
        }
    }
}