package com.sports.sob.newsmatch.centr.ssn

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sports.sob.newsmatch.centr.ssn.databinding.FragmentMainBinding
import com.sports.sob.newsmatch.centr.ssn.util.SpUtil


class qwewqrqrfgllbkbw: Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // This callback will only be called when MyFragment is at least Started.
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true /* enabled by default */) {
                override fun handleOnBackPressed() {
                    requireActivity().finishAffinity()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)

        // The callback can be enabled or disabled here or in handleOnBackPressed()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = FragmentMainBinding.inflate(inflater, container, false)

        val db = SpUtil(requireActivity())

        if (db.getLink() == "null" || db.getLink() == ""){
            b.cardBet.visibility = View.GONE
            b.textBet.visibility = View.GONE
        }

        b.news.setOnClickListener {
            findNavController().navigate(R.id.dsasdsadasdasd)
        }
        b.matches.setOnClickListener {
            findNavController().navigate(R.id.fsafasffasfa)
        }
        b.btnadsdas.setOnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder
                .setColorScheme(CustomTabsIntent.COLOR_SCHEME_DARK)
                .setUrlBarHidingEnabled(false)
                .build()

            try {
                customTabsIntent.launchUrl(
                    requireActivity(),
                    Uri.parse(db.getLink())
                )

            }catch (e: Exception){
                Toast.makeText(requireContext(), "Произошла ошибка", Toast.LENGTH_SHORT).show()
            }
        }



        return b.root
    }
}