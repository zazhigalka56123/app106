package com.sports.sob.newsmatch.centr.ssn.news

import com.sports.sob.newsmatch.centr.ssn.newsmodels.NewsDb
import retrofit2.Response
import retrofit2.http.*


interface ApiNewsService {

    @GET("/api/1/news")
    suspend fun afafsaffaafafafa123ddafssa(
        @Query("size") count: Int,
        @Query("language") language: String,
        @Query("apikey") apikey: String,
    ): Response<NewsDb>
}