package com.sports.sob.newsmatch.centr.ssn.rv2

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.sports.sob.newsmatch.centr.ssn.News
import com.sports.sob.newsmatch.centr.ssn.R
import com.sports.sob.newsmatch.centr.ssn.databinding.NewsBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class MAdapter2 : ListAdapter<News, MHolder2>(DCallback2()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MHolder2(getViewBinding(parent))

    override fun onBindViewHolder(viewHolder: MHolder2, position: Int) {
        val news = getItem(position)

        with(viewHolder.binding) {

            twercdvx.text = news.title
            description.text = news.description

            if (news.imgUrl.isNullOrEmpty())
                img.visibility = View.GONE

            var s = "Авторы: "
            var f = false
            news.creator?.forEach {
                s += "$it "
                f = true
            }

            if (!f)
                s += "-"
            ttt.text = s
            ttt2.text = "Источник: ${news.link}"
//            ttt2.movementMethod = LinkMovementMethod.getInstance();

            Glide
                .with(this.root.context)
                .load(news.imgUrl)
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.back)
                        .error(R.drawable.back)
                ).into(this.img)
        }
    }


    private fun getViewBinding(viewGroup: ViewGroup) : NewsBinding =
        NewsBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)

}