package com.sports.sob.newsmatch.centr.ssn.rv2

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.sports.sob.newsmatch.centr.ssn.databinding.ItemBinding
import com.sports.sob.newsmatch.centr.ssn.databinding.NewsBinding


class MHolder2(val binding: NewsBinding) : RecyclerView.ViewHolder(binding.root) {

    fun root(): View {
        return binding.root
    }

    fun getTTT(): View = binding.ttt

    fun getImg(): View = binding.img

    fun getDesc(): View = binding.description

    fun getTwercdvx(): View = binding.twercdvx

}

