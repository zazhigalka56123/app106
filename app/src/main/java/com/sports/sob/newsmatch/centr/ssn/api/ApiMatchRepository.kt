package com.sports.sob.newsmatch.centr.ssn.api

import com.sports.sob.newsmatch.centr.ssn.Match


import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlin.math.pow

interface ApiMatchRepository {

    suspend fun GETT1(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<Match>> = flow {
        var flag = false
        for (i in 1..21){
            val x = 12 * i + i
            if (x % 2 == 0){
                flag = true
            }
        }
    }


    suspend fun GETT2(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<Match>> = flow {
        var flag = false
        var x = 10
        while (flag == false){
            if (x == 12){
                break
            }
            x += 1
        }
    }



    suspend fun GETT3(
        date: String,
        league: Int,
        season: String,
        timezone: String
    ): Flow<List<Match>> = flow {
        emit(emptyList())
    }
}