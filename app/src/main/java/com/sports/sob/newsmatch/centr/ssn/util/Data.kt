package com.sports.sob.newsmatch.centr.ssn.util

object Data {
    const val SP_KEY = "com.sports.sob.newsmatch.centr.ssn.util.sp.key2"
    const val MAIN_KEY = "uri"
    const val IS_GET_KEY = "getUri"

    const val tableName = "Sportcob_news_106"
    const val objectId = "FCE4B88B-3EC7-458A-87D7-3BE75BDE4875"
    const val privacyLink = "https://docs.google.com/document/d/1vUJGig5i9YNr6Opr8r5UJ79IRbMwxVRKQXe16b6VYkI"
}